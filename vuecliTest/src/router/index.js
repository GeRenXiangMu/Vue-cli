import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Hi from '@/components/Hi'
import goods from '@/components/goods/goods'
import ratings from '@/components/ratings/ratings'
import seller from '@/components/seller/seller'
Vue.use(Router)

export default new Router({
  linkActiveClass:"active",//配置默认激活的class类
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      redirect:'/goods'
    },{
      path: '/Hi',
      name: 'Hi',
      component: Hi
    }
    ,{
      path: '/goods',
      name: 'goods',
      component: goods
    },{
      path: '/ratings',
      name: 'ratings',
      component: ratings
    }
    ,{
      path: '/seller',
      name: 'seller',
      component: seller
    }
  ]
})
